// Fill out your copyright notice in the Description page of Project Settings.

#include "SensorFusionEngine.h"
#include "Sensor.h"
#include "Paths.h"
#include "SensorFusionSettings.h"
#include "HAL/PlatformFilemanager.h"
#include "GenericPlatform/GenericPlatformFile.h"
#include "Misc/FileHelper.h"

// SensorFusion General Log
DEFINE_LOG_CATEGORY(LogSensorFusion);

SensorFusionEngine::SensorFusionEngine()
{
	m_hThread = INVALID_HANDLE_VALUE;
	m_sensorCnt = 0;
}

SensorFusionEngine::~SensorFusionEngine()
{
	Disconnect();
}

bool SensorFusionEngine::Connect()
{
	UE_LOG(LogSensorFusion, Display, TEXT("Connecting..."));

	FString COMPort = LoadCOMPort();
	UE_LOG(LogSensorFusion, Display, TEXT("COM Port (%s)"), *COMPort);
	m_comPort = COMPort;

	// DefaultGame.ini 로 부터 설정 값을 읽어 온다.
	if (!LoadSettings()) {
		return false;
	}

	// 센서 수신기의 COM 포트를 연다.
	if (m_serialPort.OpenPort(*m_comPort)) {
		UE_LOG(LogSensorFusion, Display, TEXT("Port is opened. (%s)"), *m_comPort);

		m_serialPort.ConfigurePort(CBR_115200, 8, false, NOPARITY, ONESTOPBIT);
		m_serialPort.SetCommunicationTimeouts(100, 100, 1000, 0, 0);
	}
	else {
		UE_LOG(LogSensorFusion, Error, TEXT("Failed to open port (%s)"), *m_comPort);
		return false;
	}

	// 센서 수신기로 부터 센서 값을 읽어오는 thread 를 생성한다.
	
	m_hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ReadThread, this, 0, NULL);

	UE_LOG(LogSensorFusion, Display, TEXT("Connected"));

	return true;
}

FString SensorFusionEngine::LoadCOMPort()
{
	FString Dir = FPaths::ProjectContentDir() + TEXT("Port/COM.txt");
	FString result;
	FFileHelper::LoadFileToString(result, *Dir);
	result = TEXT("COM") + result;
	return result;
}

bool SensorFusionEngine::LoadSettings()
{
	// 이미 Setting 값을 Load 함.
	if (m_sensorData.Num() > 0)
		return true;

	USensorFusionSettings* Settings = GetMutableDefault<USensorFusionSettings>();

	if (!Settings)
		return false;

	m_sensorCnt = Settings->Sensors.Num();

	FString strSensorID;
	for (int i = 0; i < m_sensorCnt; i++)
	{
		strSensorID = Settings->Sensors[i];
		UE_LOG(LogSensorFusion, Display, TEXT("Sensor id (%s)"), *strSensorID);
		SensorData *pData = new SensorData(strSensorID);
		m_sensorData.Add(pData);
	}

	return true;
}

void SensorFusionEngine::CloseConnection()
{
	UE_LOG(LogSensorFusion, Warning, TEXT("+++ CloseConnection"));

	if (m_hThread != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_hThread);
		m_hThread = INVALID_HANDLE_VALUE;
	}

	m_serialPort.ClosePort();
}

void SensorFusionEngine::Disconnect()
{
	UE_LOG(LogSensorFusion, Warning, TEXT("+++ Disconnected"));

	// Close thread and port
	CloseConnection();

	// Release sensor data
	for (int i = 0; i < m_sensorData.Num(); i++)
	{
		SensorData *pData = m_sensorData[i];
		delete pData;
		pData = NULL;
	}
	m_sensorData.Empty();
	m_sensorCnt = 0;
}

bool SensorFusionEngine::IsConnected()
{
	return m_serialPort.IsPortOpened();
}

bool SensorFusionEngine::GetSensorRotation(int sensorIdx, FRotator & rotation, bool bRawData)
{
	SensorData* pData = NULL;
	if (!ValidateData(sensorIdx, &pData))
		return false;

	bool ret = SensorDataToFRotator(pData, rotation, false, bRawData);

	return ret;
}

bool SensorFusionEngine::GetSensorAccel(int sensorIdx, FVector & accel, bool bRawData)
{
	SensorData* pData = NULL;
	if (!ValidateData(sensorIdx, &pData))
		return false;

	accel = pData->Data.Acceleration;

	return true;
}

bool SensorFusionEngine::GetSensorGyro(int sensorIdx, FVector & gyro, bool bRawData)
{
	SensorData* pData = NULL;
	if (!ValidateData(sensorIdx, &pData))
		return false;
	
	gyro = pData->Data.Gyro;

	return true;
}

bool SensorFusionEngine::GetSensorBattery(int sensorIdx, int & battery, bool bRawData)
{
	SensorData* pData = NULL;
	if (!ValidateData(sensorIdx, &pData))
		return false;

	battery = pData->Data.Battery;

	return true;
}

bool SensorFusionEngine::BindRotation(int sensorIdx)
{
	bool bRet = true;

	if (!IsConnected()) {
		UE_LOG(LogSensorFusion, Error, TEXT("Disconnected.."));
		return false;
	}

	SensorData *pData = m_sensorData[sensorIdx];
	if (pData != NULL) {
		if (!pData->SetBindingAngle()) {
			UE_LOG(LogSensorFusion, Error, TEXT("Bind sensor(%s)"), *pData->Data.SensorID);
			bRet = false;
		}
#if 1 //for debug
		else
		{
			UE_LOG(LogSensorFusion, Warning, TEXT("Binding rotation : ID[%s] Euler[%s]"),
				*pData->Data.SensorID, *pData->Data.Euler.ToString());
		}
#endif
	}

	if (bRet)
		UE_LOG(LogSensorFusion, Warning, TEXT("Success to bind sensor (%s)"), *pData->Data.SensorID);
	return bRet;
}

bool SensorFusionEngine::IsRotationBinded(int sensorIdx)
{
	SensorData *pData = m_sensorData[sensorIdx];
	bool bRet = true;

	if (!IsConnected())
		bRet = false;

	if (pData)
	{
		if (!pData->IsBinded())
			bRet = false;
	}

	if (bRet) {
		UE_LOG(LogSensorFusion, Warning, TEXT("IsRotationBinded (true)"));
	}
	else {
		UE_LOG(LogSensorFusion, Error, TEXT("IsRotationBinded (false"));
	}

	return bRet;
}

DWORD SensorFusionEngine::ReadThread(LPVOID lpParameter)
{
	SensorFusionEngine *pClass = (SensorFusionEngine*)lpParameter;

	while (1) {
		if (!pClass->ReadData()) {
			break;
		}
	}

	return 0;
}

int SensorFusionEngine::ReadData()
{
	bool bRoot = false;
	FString string_buffer;

	// 이 함수를 통해서 파라미터로 들어온 string_buffer에 저장합니다.
	if (!m_serialPort.ReadByteStr(string_buffer))
	{
		UE_LOG(LogSensorFusion, Error, TEXT("Fail to read data."));
		CloseConnection();
		return 0;
	}

	UE_LOG(LogSensorFusion, Display, TEXT("Read data: %s"), *string_buffer);

	// 파싱 함수를 통해 출력값을 파싱한다. 
	m_serialPort.Parsing(string_buffer);

	SensorData *pData = NULL;
	for (int i = 0; i < m_sensorData.Num(); i++)
	{
		pData = m_sensorData[i];
		if (m_serialPort.Data.SensorID == pData->Data.SensorID)
			break;
		pData = NULL;
	}

	if (pData != NULL)
	{
		pData->SetSensorValue(m_serialPort.Data);
		return 1;
	}

	UE_LOG(LogSensorFusion, Error, TEXT("No matching sonsor id (%s)"), *m_serialPort.Data.SensorID);
	return 1;
}

bool SensorFusionEngine::ValidateData(int sensorIdx, SensorData ** Data)
{

	// check the validation of input parameters
	if (sensorIdx < 0 || sensorIdx >= m_sensorData.Num())
	{
		UE_LOG(LogSensorFusion, Warning, TEXT("Invalid sensor no (%d)"), sensorIdx);
		return false;
	}

	if (!IsConnected())
	{
		UE_LOG(LogSensorFusion, Error, TEXT("Port(%s) is not opened."), *m_comPort);
		return false;
	}

	*Data = m_sensorData[sensorIdx];
	return *Data != NULL;
}

bool SensorFusionEngine::SensorDataToFRotator(SensorData *pData, FRotator& targetTransform, bool bRoot, bool bRawData)
{
	if (pData == NULL)
		return false;

	FVector vecRot = { 0.f, 0.f, 0.f };
#if 0
	if (bRoot)
		vecRot = pData->GetMyOriginalRotaion();
	else
		vecRot = pData->GetDiffAngle();
#else  // BindRotation 적용
	if (bRawData)
		vecRot = pData->GetMyOriginalRotaion();
	else
		vecRot = pData->GetAngle();
#endif
	FRotator rot = FRotator(vecRot.Y, vecRot.Z, vecRot.X);
	targetTransform = rot;
	/*FQuat quatRot(rot);
	targetTransform = FRotator(quatRot);*/

#if 1 // for debug
	{
		FQuat qrot = targetTransform.Quaternion();
		FVector tmpRot = qrot.Euler();
		UE_LOG(LogSensorFusion, Warning, TEXT("id[%s] euler[%s] quat[%s]")
			, *pData->Data.SensorID, *tmpRot.ToString(), *qrot.ToString());
	}
#endif
	return true;
}
