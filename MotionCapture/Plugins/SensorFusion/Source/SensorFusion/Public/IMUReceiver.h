// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Serialization/StructuredArchive.h"
#include "IMUReceiver.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRotationReceivedDelegate, const FRotator&, Rotation);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAccelerationReceivedDelegate, const FVector&, Acceleration);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGyroReceivedDelegate, const FVector&, Gyro);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBatteryReceivedDelegate, int32, Battery);


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SENSORFUSION_API UIMUReceiver : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UIMUReceiver();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	FString RotatorToCSVString(const FRotator& rotation) const;
	FString VectorToCSVString(const FVector& vector) const;

	void WriteDataToCSV(FArchive& Ar, float TimeStamp, const FRotator& Rotation, const FVector& Acceleration, const FVector& Gyro, int32 Battery);

	bool ReadDataFromCSV(FArchive& Ar, float* outTime, FRotator* outRotation, FVector* outAccel, FVector* outGyro, int32* outBattery);

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void TryToConnectToSensor();

	UPROPERTY(BlueprintAssignable, Category = "IMU Receiver")
		FRotationReceivedDelegate RotationReceived;

	UPROPERTY(BlueprintAssignable, Category = "IMU Receiver")
		FAccelerationReceivedDelegate AccelerationReceived;

	UPROPERTY(BlueprintAssignable, Category = "IMU Receiver")
		FGyroReceivedDelegate GyroReceived;

	UPROPERTY(BlueprintAssignable, Category = "IMU Receiver")
		FBatteryReceivedDelegate BatteryReceived;

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "IMU Receiver")
		int32 IMU_Id = 0;

	UPROPERTY(EditAnywhere, Category = "IMU Receiver")
		bool bUseRawData = false;

	/*
	Debugging Level
	0/Default: No Debugging Logs
	1: IMU Sensor Data Logs
	2: IMU Acceleration Direction Motion Logs
	*/
	UPROPERTY(EditAnywhere, Category = "IMU Receiver")
		int32 DebugLevel = 0;

	
	// CSV RECORDER

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CSV Output")
		bool bEnableCSVRecording = false;

	UPROPERTY(EditAnywhere, Category = "CSV Output", meta = (EditCondition = "bEnableCSVRecording"))
		FString RecorderFilename;

	UPROPERTY(EditAnywhere, Category = "CSV Output", meta = (EditCondition = "bEnableCSVRecording"))
		float RecordingRate = 0.01f;

	float RecordingTimer = 0.f;

	float RecordingTotalTime = 0.f;

	FArchive* FileWriter = nullptr;


	//CSV READER


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CSV Input")
	bool bEnableCSVReader = false;

	UPROPERTY(EditAnywhere, Category = "CSV Input", meta = (EditCondition = "bEnableCSVReader"))
	FString ReaderFilename;

	float ReadingTotalTime = 0.f;

	FArchive* FileReader = nullptr;

	float LastReadTime;
	int64 ReadFileSize;
	int64 ReadBytes;

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sensor Value")
	FRotator Rotation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sensor Value")
	FVector Acceleration;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sensor Value")
	FVector Gyro;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sensor Value")
	int Battery;
	
};
