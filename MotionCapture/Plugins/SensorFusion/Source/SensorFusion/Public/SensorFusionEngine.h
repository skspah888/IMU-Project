// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Sensor.h"
#include "SensorData.h"

// SensorFusion General Log
DECLARE_LOG_CATEGORY_EXTERN(LogSensorFusion, Log, All);

/**
 * @class SensorFusionEngine
 *
 */
class SENSORFUSION_API SensorFusionEngine
{
public:
	SensorFusionEngine();
	~SensorFusionEngine();

	bool Connect();
	void Disconnect();
	bool IsConnected();



	bool ValidateData(int sensorIdx, SensorData** Data);

	bool GetSensorRotation(int sensorIdx, FRotator& rotation, bool bRawData=false);
	bool GetSensorAccel(int sensorIdx, FVector& accel, bool bRawData = false);
	bool GetSensorGyro(int sensorIdx, FVector& gyro, bool bRawData = false);
	bool GetSensorBattery(int sensorIdx, int& battery, bool bRawData = false);

	bool BindRotation(int sensorIdx);
	bool IsRotationBinded(int sensorIdx);

	static DWORD ReadThread(LPVOID lpParameter);
	int ReadData();

private:
	FString LoadCOMPort();

	bool LoadSettings();
	void CloseConnection();

	bool SensorDataToFRotator(SensorData *pData, FRotator& targetTransform, bool bRoot=false, bool bRawData=false);

	HANDLE m_hThread;

	int	m_sensorCnt;
	FString m_comPort;
	SensorData m_rootData;
	TArray<SensorData *> m_sensorData;

public:
	Sensor m_serialPort;//
};
