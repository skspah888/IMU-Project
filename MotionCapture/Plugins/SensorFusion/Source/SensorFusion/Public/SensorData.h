// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Sensor.h"
/**
 * 
 */

class SENSORFUSION_API SensorData
{
public:
	SensorData();
	SensorData(FString sensorId);
	~SensorData();

	FSensorInfoStruct Data;

private:

	float BindYaw;
	FVector Angle{ 0.f };
	FVector ParentRotationEuler{ 1.f };

	// 2018 8 2 Update
	FVector MyRotationEuler{ 0.f };
	FVector MyRotationDiffParent{ 0.f };

	bool bBinded;
	bool bConnected;

public:

	void SetSensorValue(FSensorInfoStruct& parseData);
	bool SetBindingAngle();
	FVector GetAngle();
	bool IsBinded();

	FVector GetMyOriginalRotaion();
};
