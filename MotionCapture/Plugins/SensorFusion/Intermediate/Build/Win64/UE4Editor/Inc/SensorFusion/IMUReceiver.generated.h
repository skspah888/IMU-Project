// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
struct FRotator;
#ifdef SENSORFUSION_IMUReceiver_generated_h
#error "IMUReceiver.generated.h already included, missing '#pragma once' in IMUReceiver.h"
#endif
#define SENSORFUSION_IMUReceiver_generated_h

#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_14_DELEGATE \
struct _Script_SensorFusion_eventBatteryReceivedDelegate_Parms \
{ \
	int32 Battery; \
}; \
static inline void FBatteryReceivedDelegate_DelegateWrapper(const FMulticastScriptDelegate& BatteryReceivedDelegate, int32 Battery) \
{ \
	_Script_SensorFusion_eventBatteryReceivedDelegate_Parms Parms; \
	Parms.Battery=Battery; \
	BatteryReceivedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_13_DELEGATE \
struct _Script_SensorFusion_eventGyroReceivedDelegate_Parms \
{ \
	FVector Gyro; \
}; \
static inline void FGyroReceivedDelegate_DelegateWrapper(const FMulticastScriptDelegate& GyroReceivedDelegate, FVector const& Gyro) \
{ \
	_Script_SensorFusion_eventGyroReceivedDelegate_Parms Parms; \
	Parms.Gyro=Gyro; \
	GyroReceivedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_12_DELEGATE \
struct _Script_SensorFusion_eventAccelerationReceivedDelegate_Parms \
{ \
	FVector Acceleration; \
}; \
static inline void FAccelerationReceivedDelegate_DelegateWrapper(const FMulticastScriptDelegate& AccelerationReceivedDelegate, FVector const& Acceleration) \
{ \
	_Script_SensorFusion_eventAccelerationReceivedDelegate_Parms Parms; \
	Parms.Acceleration=Acceleration; \
	AccelerationReceivedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_11_DELEGATE \
struct _Script_SensorFusion_eventRotationReceivedDelegate_Parms \
{ \
	FRotator Rotation; \
}; \
static inline void FRotationReceivedDelegate_DelegateWrapper(const FMulticastScriptDelegate& RotationReceivedDelegate, FRotator const& Rotation) \
{ \
	_Script_SensorFusion_eventRotationReceivedDelegate_Parms Parms; \
	Parms.Rotation=Rotation; \
	RotationReceivedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_RPC_WRAPPERS
#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUIMUReceiver(); \
	friend struct Z_Construct_UClass_UIMUReceiver_Statics; \
public: \
	DECLARE_CLASS(UIMUReceiver, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SensorFusion"), NO_API) \
	DECLARE_SERIALIZER(UIMUReceiver)


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUIMUReceiver(); \
	friend struct Z_Construct_UClass_UIMUReceiver_Statics; \
public: \
	DECLARE_CLASS(UIMUReceiver, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SensorFusion"), NO_API) \
	DECLARE_SERIALIZER(UIMUReceiver)


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UIMUReceiver(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UIMUReceiver) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UIMUReceiver); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UIMUReceiver); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UIMUReceiver(UIMUReceiver&&); \
	NO_API UIMUReceiver(const UIMUReceiver&); \
public:


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UIMUReceiver(UIMUReceiver&&); \
	NO_API UIMUReceiver(const UIMUReceiver&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UIMUReceiver); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UIMUReceiver); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UIMUReceiver)


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_PRIVATE_PROPERTY_OFFSET
#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_17_PROLOG
#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_PRIVATE_PROPERTY_OFFSET \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_RPC_WRAPPERS \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_INCLASS \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_PRIVATE_PROPERTY_OFFSET \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_INCLASS_NO_PURE_DECLS \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SENSORFUSION_API UClass* StaticClass<class UIMUReceiver>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_IMUReceiver_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
