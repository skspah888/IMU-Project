// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SensorFusion/Public/SensorFusionBPLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSensorFusionBPLibrary() {}
// Cross Module References
	SENSORFUSION_API UClass* Z_Construct_UClass_USensorFusionBPLibrary_NoRegister();
	SENSORFUSION_API UClass* Z_Construct_UClass_USensorFusionBPLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_SensorFusion();
	SENSORFUSION_API UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation();
	SENSORFUSION_API UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_Connect();
	SENSORFUSION_API UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_Disconnect();
	SENSORFUSION_API UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	SENSORFUSION_API UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery();
	SENSORFUSION_API UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro();
	SENSORFUSION_API UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	SENSORFUSION_API UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected();
	SENSORFUSION_API UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded();
	SENSORFUSION_API UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_PrintLog();
// End Cross Module References
	void USensorFusionBPLibrary::StaticRegisterNativesUSensorFusionBPLibrary()
	{
		UClass* Class = USensorFusionBPLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "BindRotation", &USensorFusionBPLibrary::execBindRotation },
			{ "Connect", &USensorFusionBPLibrary::execConnect },
			{ "Disconnect", &USensorFusionBPLibrary::execDisconnect },
			{ "GetSensorAcceleration", &USensorFusionBPLibrary::execGetSensorAcceleration },
			{ "GetSensorBattery", &USensorFusionBPLibrary::execGetSensorBattery },
			{ "GetSensorGyro", &USensorFusionBPLibrary::execGetSensorGyro },
			{ "GetSensorRotation", &USensorFusionBPLibrary::execGetSensorRotation },
			{ "IsConnected", &USensorFusionBPLibrary::execIsConnected },
			{ "IsRotationBinded", &USensorFusionBPLibrary::execIsRotationBinded },
			{ "PrintLog", &USensorFusionBPLibrary::execPrintLog },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics
	{
		struct SensorFusionBPLibrary_eventBindRotation_Parms
		{
			int32 idxSensor;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_idxSensor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SensorFusionBPLibrary_eventBindRotation_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SensorFusionBPLibrary_eventBindRotation_Parms), &Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::NewProp_idxSensor = { "idxSensor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SensorFusionBPLibrary_eventBindRotation_Parms, idxSensor), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::NewProp_idxSensor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::Function_MetaDataParams[] = {
		{ "Category", "SensorFusion" },
		{ "DisplayName", "BindRotation" },
		{ "Keywords", "" },
		{ "ModuleRelativePath", "Public/SensorFusionBPLibrary.h" },
		{ "ToolTip", "Sensor?? ???\xcf\xbf? Rotation?? Bind?\xd5\xb4\xcf\xb4?." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USensorFusionBPLibrary, nullptr, "BindRotation", sizeof(SensorFusionBPLibrary_eventBindRotation_Parms), Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics
	{
		struct SensorFusionBPLibrary_eventConnect_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SensorFusionBPLibrary_eventConnect_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SensorFusionBPLibrary_eventConnect_Parms), &Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics::Function_MetaDataParams[] = {
		{ "Category", "SensorFusion" },
		{ "DisplayName", "ConnectToSensor" },
		{ "Keywords", "" },
		{ "ModuleRelativePath", "Public/SensorFusionBPLibrary.h" },
		{ "ToolTip", "???????? ?????? ?\xc3\xb5??\xd5\xb4\xcf\xb4?." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USensorFusionBPLibrary, nullptr, "Connect", sizeof(SensorFusionBPLibrary_eventConnect_Parms), Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_Connect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USensorFusionBPLibrary_Connect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USensorFusionBPLibrary_Disconnect_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USensorFusionBPLibrary_Disconnect_Statics::Function_MetaDataParams[] = {
		{ "Category", "SensorFusion" },
		{ "DisplayName", "DisconnectFromSensor" },
		{ "Keywords", "" },
		{ "ModuleRelativePath", "Public/SensorFusionBPLibrary.h" },
		{ "ToolTip", "???????? ?????? ?????\xcf\xb4?." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USensorFusionBPLibrary_Disconnect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USensorFusionBPLibrary, nullptr, "Disconnect", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USensorFusionBPLibrary_Disconnect_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_Disconnect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_Disconnect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USensorFusionBPLibrary_Disconnect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics
	{
		struct SensorFusionBPLibrary_eventGetSensorAcceleration_Parms
		{
			int32 idxSensor;
			FVector acceleration;
			bool bRawData;
		};
		static void NewProp_bRawData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRawData;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_acceleration;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_idxSensor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::NewProp_bRawData_SetBit(void* Obj)
	{
		((SensorFusionBPLibrary_eventGetSensorAcceleration_Parms*)Obj)->bRawData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::NewProp_bRawData = { "bRawData", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SensorFusionBPLibrary_eventGetSensorAcceleration_Parms), &Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::NewProp_bRawData_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::NewProp_acceleration = { "acceleration", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SensorFusionBPLibrary_eventGetSensorAcceleration_Parms, acceleration), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::NewProp_idxSensor = { "idxSensor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SensorFusionBPLibrary_eventGetSensorAcceleration_Parms, idxSensor), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::NewProp_bRawData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::NewProp_acceleration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::NewProp_idxSensor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::Function_MetaDataParams[] = {
		{ "Category", "SensorFusion" },
		{ "CPP_Default_bRawData", "false" },
		{ "ModuleRelativePath", "Public/SensorFusionBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USensorFusionBPLibrary, nullptr, "GetSensorAcceleration", sizeof(SensorFusionBPLibrary_eventGetSensorAcceleration_Parms), Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics
	{
		struct SensorFusionBPLibrary_eventGetSensorBattery_Parms
		{
			int32 idxSensor;
			int32 battery;
			bool bRawData;
		};
		static void NewProp_bRawData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRawData;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_battery;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_idxSensor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::NewProp_bRawData_SetBit(void* Obj)
	{
		((SensorFusionBPLibrary_eventGetSensorBattery_Parms*)Obj)->bRawData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::NewProp_bRawData = { "bRawData", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SensorFusionBPLibrary_eventGetSensorBattery_Parms), &Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::NewProp_bRawData_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::NewProp_battery = { "battery", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SensorFusionBPLibrary_eventGetSensorBattery_Parms, battery), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::NewProp_idxSensor = { "idxSensor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SensorFusionBPLibrary_eventGetSensorBattery_Parms, idxSensor), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::NewProp_bRawData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::NewProp_battery,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::NewProp_idxSensor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::Function_MetaDataParams[] = {
		{ "Category", "SensorFusion" },
		{ "CPP_Default_bRawData", "false" },
		{ "ModuleRelativePath", "Public/SensorFusionBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USensorFusionBPLibrary, nullptr, "GetSensorBattery", sizeof(SensorFusionBPLibrary_eventGetSensorBattery_Parms), Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics
	{
		struct SensorFusionBPLibrary_eventGetSensorGyro_Parms
		{
			int32 idxSensor;
			FVector gyro;
			bool bRawData;
		};
		static void NewProp_bRawData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRawData;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_gyro;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_idxSensor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::NewProp_bRawData_SetBit(void* Obj)
	{
		((SensorFusionBPLibrary_eventGetSensorGyro_Parms*)Obj)->bRawData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::NewProp_bRawData = { "bRawData", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SensorFusionBPLibrary_eventGetSensorGyro_Parms), &Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::NewProp_bRawData_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::NewProp_gyro = { "gyro", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SensorFusionBPLibrary_eventGetSensorGyro_Parms, gyro), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::NewProp_idxSensor = { "idxSensor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SensorFusionBPLibrary_eventGetSensorGyro_Parms, idxSensor), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::NewProp_bRawData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::NewProp_gyro,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::NewProp_idxSensor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::Function_MetaDataParams[] = {
		{ "Category", "SensorFusion" },
		{ "CPP_Default_bRawData", "false" },
		{ "ModuleRelativePath", "Public/SensorFusionBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USensorFusionBPLibrary, nullptr, "GetSensorGyro", sizeof(SensorFusionBPLibrary_eventGetSensorGyro_Parms), Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics
	{
		struct SensorFusionBPLibrary_eventGetSensorRotation_Parms
		{
			int32 idxSensor;
			FRotator Rotation;
			bool bRawData;
		};
		static void NewProp_bRawData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRawData;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_idxSensor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::NewProp_bRawData_SetBit(void* Obj)
	{
		((SensorFusionBPLibrary_eventGetSensorRotation_Parms*)Obj)->bRawData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::NewProp_bRawData = { "bRawData", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SensorFusionBPLibrary_eventGetSensorRotation_Parms), &Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::NewProp_bRawData_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SensorFusionBPLibrary_eventGetSensorRotation_Parms, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::NewProp_idxSensor = { "idxSensor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SensorFusionBPLibrary_eventGetSensorRotation_Parms, idxSensor), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::NewProp_bRawData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::NewProp_idxSensor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::Function_MetaDataParams[] = {
		{ "Category", "SensorFusion" },
		{ "CPP_Default_bRawData", "false" },
		{ "ModuleRelativePath", "Public/SensorFusionBPLibrary.h" },
		{ "ToolTip", "?\xd6\xbe??? ?\xce\xb5????? ?\xd8\xb4??\xcf\xb4? ?????? Transform???? \xc8\xae???\xd5\xb4\xcf\xb4?." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USensorFusionBPLibrary, nullptr, "GetSensorRotation", sizeof(SensorFusionBPLibrary_eventGetSensorRotation_Parms), Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics
	{
		struct SensorFusionBPLibrary_eventIsConnected_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SensorFusionBPLibrary_eventIsConnected_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SensorFusionBPLibrary_eventIsConnected_Parms), &Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics::Function_MetaDataParams[] = {
		{ "Category", "SensorFusion" },
		{ "DisplayName", "IsSensorConnected" },
		{ "Keywords", "" },
		{ "ModuleRelativePath", "Public/SensorFusionBPLibrary.h" },
		{ "ToolTip", "???????? ???\xe1\xbf\xa9?\xce\xb8? \xc8\xae???\xd5\xb4\xcf\xb4?." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USensorFusionBPLibrary, nullptr, "IsConnected", sizeof(SensorFusionBPLibrary_eventIsConnected_Parms), Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics
	{
		struct SensorFusionBPLibrary_eventIsRotationBinded_Parms
		{
			int32 idxSensor;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_idxSensor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SensorFusionBPLibrary_eventIsRotationBinded_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SensorFusionBPLibrary_eventIsRotationBinded_Parms), &Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::NewProp_idxSensor = { "idxSensor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SensorFusionBPLibrary_eventIsRotationBinded_Parms, idxSensor), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::NewProp_idxSensor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::Function_MetaDataParams[] = {
		{ "Category", "SensorFusion" },
		{ "DisplayName", "IsRotationBinded" },
		{ "Keywords", "" },
		{ "ModuleRelativePath", "Public/SensorFusionBPLibrary.h" },
		{ "ToolTip", "Sensor?? Rotation?? Bind ???\xce\xb8? \xc8\xae???\xd5\xb4\xcf\xb4?." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USensorFusionBPLibrary, nullptr, "IsRotationBinded", sizeof(SensorFusionBPLibrary_eventIsRotationBinded_Parms), Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USensorFusionBPLibrary_PrintLog_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USensorFusionBPLibrary_PrintLog_Statics::Function_MetaDataParams[] = {
		{ "Category", "SensorFusion" },
		{ "DisplayName", "PrintLog" },
		{ "Keywords", "" },
		{ "ModuleRelativePath", "Public/SensorFusionBPLibrary.h" },
		{ "ToolTip", "Log?? ?????\xd5\xb4\xcf\xb4?." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USensorFusionBPLibrary_PrintLog_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USensorFusionBPLibrary, nullptr, "PrintLog", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USensorFusionBPLibrary_PrintLog_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_USensorFusionBPLibrary_PrintLog_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USensorFusionBPLibrary_PrintLog()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USensorFusionBPLibrary_PrintLog_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_USensorFusionBPLibrary_NoRegister()
	{
		return USensorFusionBPLibrary::StaticClass();
	}
	struct Z_Construct_UClass_USensorFusionBPLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USensorFusionBPLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_SensorFusion,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USensorFusionBPLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USensorFusionBPLibrary_BindRotation, "BindRotation" }, // 2101417642
		{ &Z_Construct_UFunction_USensorFusionBPLibrary_Connect, "Connect" }, // 824879375
		{ &Z_Construct_UFunction_USensorFusionBPLibrary_Disconnect, "Disconnect" }, // 1443806786
		{ &Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorAcceleration, "GetSensorAcceleration" }, // 2380438816
		{ &Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorBattery, "GetSensorBattery" }, // 1504279549
		{ &Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorGyro, "GetSensorGyro" }, // 3071724804
		{ &Z_Construct_UFunction_USensorFusionBPLibrary_GetSensorRotation, "GetSensorRotation" }, // 2667722099
		{ &Z_Construct_UFunction_USensorFusionBPLibrary_IsConnected, "IsConnected" }, // 3493605939
		{ &Z_Construct_UFunction_USensorFusionBPLibrary_IsRotationBinded, "IsRotationBinded" }, // 2542423888
		{ &Z_Construct_UFunction_USensorFusionBPLibrary_PrintLog, "PrintLog" }, // 3990725843
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USensorFusionBPLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SensorFusionBPLibrary.h" },
		{ "ModuleRelativePath", "Public/SensorFusionBPLibrary.h" },
		{ "ToolTip", "*      Function library class.\n*      Each function in it is expected to be static and represents blueprint node that can be called in any blueprint.\n*\n*      When declaring function you can define metadata for the node. Key function specifiers will be BlueprintPure and BlueprintCallable.\n*      BlueprintPure - means the function does not affect the owning object in any way and thus creates a node without Exec pins.\n*      BlueprintCallable - makes a function which can be executed in Blueprints - Thus it has Exec pins.\n*      DisplayName - full name of the node, shown when you mouse over the node and in the blueprint drop down menu.\n*                              Its lets you name the node using characters not allowed in C++ function names.\n*      CompactNodeTitle - the word(s) that appear on the node.\n*      Keywords -      the list of keywords that helps you to find node when you search for it using Blueprint drop-down menu.\n*                              Good example is \"Print String\" node which you can find also by using keyword \"log\".\n*      Category -      the category your node will be under in the Blueprint drop-down menu.\n*\n*      For more info on custom blueprint nodes visit documentation:\n*      https://wiki.unrealengine.com/Custom_Blueprint_Node_Creation" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USensorFusionBPLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USensorFusionBPLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USensorFusionBPLibrary_Statics::ClassParams = {
		&USensorFusionBPLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USensorFusionBPLibrary_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_USensorFusionBPLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USensorFusionBPLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USensorFusionBPLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USensorFusionBPLibrary, 3263822126);
	template<> SENSORFUSION_API UClass* StaticClass<USensorFusionBPLibrary>()
	{
		return USensorFusionBPLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USensorFusionBPLibrary(Z_Construct_UClass_USensorFusionBPLibrary, &USensorFusionBPLibrary::StaticClass, TEXT("/Script/SensorFusion"), TEXT("USensorFusionBPLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USensorFusionBPLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
