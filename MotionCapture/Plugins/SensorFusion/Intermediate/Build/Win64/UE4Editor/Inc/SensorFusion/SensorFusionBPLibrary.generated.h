// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
struct FRotator;
#ifdef SENSORFUSION_SensorFusionBPLibrary_generated_h
#error "SensorFusionBPLibrary.generated.h already included, missing '#pragma once' in SensorFusionBPLibrary.h"
#endif
#define SENSORFUSION_SensorFusionBPLibrary_generated_h

#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetSensorBattery) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_idxSensor); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_battery); \
		P_GET_UBOOL(Z_Param_bRawData); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		USensorFusionBPLibrary::GetSensorBattery(Z_Param_idxSensor,Z_Param_Out_battery,Z_Param_bRawData); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSensorGyro) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_idxSensor); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_gyro); \
		P_GET_UBOOL(Z_Param_bRawData); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		USensorFusionBPLibrary::GetSensorGyro(Z_Param_idxSensor,Z_Param_Out_gyro,Z_Param_bRawData); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSensorAcceleration) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_idxSensor); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_acceleration); \
		P_GET_UBOOL(Z_Param_bRawData); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		USensorFusionBPLibrary::GetSensorAcceleration(Z_Param_idxSensor,Z_Param_Out_acceleration,Z_Param_bRawData); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintLog) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		USensorFusionBPLibrary::PrintLog(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsRotationBinded) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_idxSensor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=USensorFusionBPLibrary::IsRotationBinded(Z_Param_idxSensor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execBindRotation) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_idxSensor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=USensorFusionBPLibrary::BindRotation(Z_Param_idxSensor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSensorRotation) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_idxSensor); \
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_Rotation); \
		P_GET_UBOOL(Z_Param_bRawData); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		USensorFusionBPLibrary::GetSensorRotation(Z_Param_idxSensor,Z_Param_Out_Rotation,Z_Param_bRawData); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsConnected) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=USensorFusionBPLibrary::IsConnected(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDisconnect) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		USensorFusionBPLibrary::Disconnect(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execConnect) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=USensorFusionBPLibrary::Connect(); \
		P_NATIVE_END; \
	}


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetSensorBattery) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_idxSensor); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_battery); \
		P_GET_UBOOL(Z_Param_bRawData); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		USensorFusionBPLibrary::GetSensorBattery(Z_Param_idxSensor,Z_Param_Out_battery,Z_Param_bRawData); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSensorGyro) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_idxSensor); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_gyro); \
		P_GET_UBOOL(Z_Param_bRawData); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		USensorFusionBPLibrary::GetSensorGyro(Z_Param_idxSensor,Z_Param_Out_gyro,Z_Param_bRawData); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSensorAcceleration) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_idxSensor); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_acceleration); \
		P_GET_UBOOL(Z_Param_bRawData); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		USensorFusionBPLibrary::GetSensorAcceleration(Z_Param_idxSensor,Z_Param_Out_acceleration,Z_Param_bRawData); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintLog) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		USensorFusionBPLibrary::PrintLog(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsRotationBinded) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_idxSensor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=USensorFusionBPLibrary::IsRotationBinded(Z_Param_idxSensor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execBindRotation) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_idxSensor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=USensorFusionBPLibrary::BindRotation(Z_Param_idxSensor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSensorRotation) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_idxSensor); \
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_Rotation); \
		P_GET_UBOOL(Z_Param_bRawData); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		USensorFusionBPLibrary::GetSensorRotation(Z_Param_idxSensor,Z_Param_Out_Rotation,Z_Param_bRawData); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsConnected) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=USensorFusionBPLibrary::IsConnected(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDisconnect) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		USensorFusionBPLibrary::Disconnect(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execConnect) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=USensorFusionBPLibrary::Connect(); \
		P_NATIVE_END; \
	}


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSensorFusionBPLibrary(); \
	friend struct Z_Construct_UClass_USensorFusionBPLibrary_Statics; \
public: \
	DECLARE_CLASS(USensorFusionBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SensorFusion"), NO_API) \
	DECLARE_SERIALIZER(USensorFusionBPLibrary)


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUSensorFusionBPLibrary(); \
	friend struct Z_Construct_UClass_USensorFusionBPLibrary_Statics; \
public: \
	DECLARE_CLASS(USensorFusionBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SensorFusion"), NO_API) \
	DECLARE_SERIALIZER(USensorFusionBPLibrary)


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USensorFusionBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USensorFusionBPLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USensorFusionBPLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USensorFusionBPLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USensorFusionBPLibrary(USensorFusionBPLibrary&&); \
	NO_API USensorFusionBPLibrary(const USensorFusionBPLibrary&); \
public:


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USensorFusionBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USensorFusionBPLibrary(USensorFusionBPLibrary&&); \
	NO_API USensorFusionBPLibrary(const USensorFusionBPLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USensorFusionBPLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USensorFusionBPLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USensorFusionBPLibrary)


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_PRIVATE_PROPERTY_OFFSET
#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_26_PROLOG
#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_PRIVATE_PROPERTY_OFFSET \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_RPC_WRAPPERS \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_INCLASS \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_PRIVATE_PROPERTY_OFFSET \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_INCLASS_NO_PURE_DECLS \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h_29_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SensorFusionBPLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SENSORFUSION_API UClass* StaticClass<class USensorFusionBPLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionBPLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
