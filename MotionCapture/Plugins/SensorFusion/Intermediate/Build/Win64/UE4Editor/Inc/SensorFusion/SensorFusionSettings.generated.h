// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SENSORFUSION_SensorFusionSettings_generated_h
#error "SensorFusionSettings.generated.h already included, missing '#pragma once' in SensorFusionSettings.h"
#endif
#define SENSORFUSION_SensorFusionSettings_generated_h

#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_RPC_WRAPPERS
#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSensorFusionSettings(); \
	friend struct Z_Construct_UClass_USensorFusionSettings_Statics; \
public: \
	DECLARE_CLASS(USensorFusionSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/SensorFusion"), NO_API) \
	DECLARE_SERIALIZER(USensorFusionSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSensorFusionSettings(); \
	friend struct Z_Construct_UClass_USensorFusionSettings_Statics; \
public: \
	DECLARE_CLASS(USensorFusionSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/SensorFusion"), NO_API) \
	DECLARE_SERIALIZER(USensorFusionSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USensorFusionSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USensorFusionSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USensorFusionSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USensorFusionSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USensorFusionSettings(USensorFusionSettings&&); \
	NO_API USensorFusionSettings(const USensorFusionSettings&); \
public:


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USensorFusionSettings(USensorFusionSettings&&); \
	NO_API USensorFusionSettings(const USensorFusionSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USensorFusionSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USensorFusionSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USensorFusionSettings)


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_PRIVATE_PROPERTY_OFFSET
#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_12_PROLOG
#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_RPC_WRAPPERS \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_INCLASS \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_INCLASS_NO_PURE_DECLS \
	MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SENSORFUSION_API UClass* StaticClass<class USensorFusionSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MotionCapture_Plugins_SensorFusion_Source_SensorFusion_Public_SensorFusionSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
