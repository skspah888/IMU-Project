// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "EngineMinimal.h"

enum BONE_NAME
{
	BONE_R_SPINE,
	BONE_R_ARM,
	BONE_R_ELBOW,
	BONE_END
};