// Fill out your copyright notice in the Description page of Project Settings.


#include "Test_Sensor.h"
#include "IMUReceiver.h"
#include <MotionControllerComponent.h>
#include <Components/SkeletalMeshComponent.h>
//#include "MyGameInstance.h"

// Sets default values
ATest_Sensor::ATest_Sensor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	arrIMU[BONE_R_ELBOW] = CreateDefaultSubobject<UIMUReceiver>(TEXT("IMUReceiver_R_ELBOW"));
	arrIMU[BONE_R_ARM] = CreateDefaultSubobject<UIMUReceiver>(TEXT("IMUReceiver_R_ARM"));
	arrIMU[BONE_R_SPINE] = CreateDefaultSubobject<UIMUReceiver>(TEXT("IMUReceiver_R_SPINE"));

	//static ConstructorHelpers::FObjectFinder<UBlueprint> BPObject(TEXT(
	//	"Blueprint'/Game/Blueprint/Animation/CalibrationPawn.CalibrationPawn_C'"));
	//if (BPObject.Object != NULL)
	//{
	//	CalibrationPawn = BPObject.Object->GeneratedClass;
	//}

	RootComponent = SceneComponent;

	iCount = 0;
	fTimerInterval = 2.f;
	IsFinish = false;

	eState = EERROR_STATE::ERRRO_NONE;
}

// Called when the game starts or when spawned
void ATest_Sensor::BeginPlay()
{
	Super::BeginPlay();

	int32 Num = 0;
	for (auto& Receiver : arrIMU)
	{
		Receiver->IMU_Id = Num;
		Receiver->DebugLevel = 0;
		Receiver->bUseRawData = true;
		Num++;
	}

	
	// 컴포넌트 등록
	CalibrationPawn->GetComponents(MotionComps);
	CalibrationPawn->GetComponents(MeshComps);

	/*for (int i = 0; i < MotionComps.Num(); ++i)
	{
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, FString::Printf(TEXT("Name %s"),
			*MotionComps[i]->GetName()));
	}

	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, FString::Printf(TEXT("Name %s"),
		*MeshComps[0]->GetName()));*/
}

void ATest_Sensor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	//GetWorld()->GetTimerManager().ClearTimer(CheckTimerHandle);
}

// Called every frame
void ATest_Sensor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	int32 Num = 0;
	for (auto& Receiver : arrIMU)
	{
		arrRotator[Num] = Receiver->Rotation;
		Num++;
	}

	if (IsFinish)
	{
		
	}

}

void ATest_Sensor::InitRotation()
{
	GEngine->ClearOnScreenDebugMessages();
	FColor Color;
	for (int i = 0; i < BONE_END; ++i)
	{
		arrInitRotator[i] = arrRotator[i];
		TestPrintLog(i, arrInitRotator[i], Color.Red, 100.f);
	}

	//// 2초마다 캘리브레이션 검사 수행
	//GetWorld()->GetTimerManager().SetTimer(CheckTimerHandle, this, &ATest_Sensor::CheckCalibration, fTimerInterval, true, 2.f);
}

void ATest_Sensor::TestPrintLog(int32 index, FRotator Rotation, FColor Color, float fTime)
{
	//GEngine->AddOnScreenDebugMessage(-1, fTime, Color, FString::Printf(TEXT("Sensor: %s, X: %f, Y: %f, Z: %f"),
		//*arrIMU[index]->GetName(), Rotation.Roll, Rotation.Pitch, Rotation.Yaw));
}

bool ATest_Sensor::CheckCalibration()
{
	// 배열에 센서 Rotator값 저장
	eState = EERROR_STATE::ERRRO_NONE;
	FColor Color;
	for (int i = 0; i < BONE_END; ++i)
	{
		arrCalibrationRotator[i] = arrRotator[i];
		TestPrintLog(i, arrCalibrationRotator[i], Color.Green, 2.f);
	}

	// arrInitRotator와 저장한 값 비교
	bool bPass = false;
	for (int i = 0; i < BONE_END; ++i)
	{
		// IMU 센서 검사
		bPass = CompareSensorRotation(arrInitRotator[i], arrCalibrationRotator[i]);
		if (!bPass)
		{
			eState = EERROR_STATE::IMU_SENSOR;

			// Fail 메세지를 띄우면서 다시 바인드 단계로 돌아가야함.
			//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Blue, FString::Printf(TEXT("Compare Failed")));

			//// 타이머 Clear
			//GetWorld()->GetTimerManager().ClearTimer(CheckTimerHandle);

			// 카운트 초기화
			iCount = 0;

			return false;

		}

		// Motion Controller 검사
		bPass = CompareMotionController();

		if (!bPass) // 값이 차이가 심함 [ 많이 움직임 ]
		{
			// Fail 메세지를 띄우면서 다시 바인드 단계로 돌아가야함.
			//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Blue, FString::Printf(TEXT("Compare Failed")));

			//// 타이머 Clear
			//GetWorld()->GetTimerManager().ClearTimer(CheckTimerHandle);

			// 카운트 초기화
			iCount = 0;
	
			return false;
		}
	}


	++iCount;

	//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Blue, FString::Printf(TEXT("%d Test Succeed"), iCount));

	return true;
}

void ATest_Sensor::CheckCalibration_Count()
{
	if (iCount >= 5)
	{
		// 전부 통과
		//GEngine->ClearOnScreenDebugMessages();

		//GetWorld()->GetTimerManager().ClearTimer(CheckTimerHandle);

		// 최종값 셋팅 [ 이 값과 모델값의 차이로 보정할 것 ]
		for (int i = 0; i < BONE_END; ++i)
		//{
		//	arrInitRotator[i] = arrRotator[i];
		//}

		//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Blue, FString::Printf(TEXT("Calibration Succeed!!!!")));
		IsFinish = true;

		// 다른 타이머 함수를 호출하던 마무리 작업 해주면 됨
		//for (int i = 0; i < BONE_END; ++i)
		//{
		//	TestPrintLog(i, arrInitRotator[i], FColor::Magenta, 100.f);
		//}

		//Cast<UMyGameInstance>(GetGameInstance())->InitRotation(arrInitRotator);

	}
}


bool ATest_Sensor::CompareSensorRotation(const FRotator& InitRotator, const FRotator& CalibrationRotator)
{
	// X Y Z 나눠서 해야함
	return InitRotator.Equals(CalibrationRotator, 10);
}

bool ATest_Sensor::CompareMotionController()
{
	bool bLeftController = Check_LeftController();

	if (bLeftController)
	{
		// 왼쪽 검사 통과
		//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Blue, FString::Printf(TEXT("Left Succeed!!!!")));
	}
	else
	{
		// 왼쪽 검사 실패
		eState = EERROR_STATE::LEFT_CONTROLLER;
		return false;
	}



	bool bRightController = Check_RightController();

	if (bRightController)
	{
		// 오른쪽 검사 통과
		//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Blue, FString::Printf(TEXT("Right Succeed!!!!")));
	}
	else
	{
		// 오른쪽 검사 실패
		eState = EERROR_STATE::RIGHT_CONTROLLER;
		return false;
	}

	return true;
}

bool ATest_Sensor::Check_LeftController()
{
	FVector MeshUpVector = MeshComps[0]->GetUpVector();
	FVector ControllerUpVector = MotionComps[0]->GetUpVector();
	FVector ControllerRightVector = MotionComps[0]->GetRightVector();

	float DotUpVector = FVector::DotProduct(MeshUpVector, ControllerUpVector);
	float DotRightVector = FVector::DotProduct(MeshUpVector, ControllerRightVector);

	if (DotUpVector >= -0.4f && DotUpVector <= 0.4f && DotRightVector >= -1.0f && DotRightVector <= -0.7f)
		return true;

	return false;
}

bool ATest_Sensor::Check_RightController()
{
	FVector MeshUpVector = MeshComps[0]->GetUpVector();
	FVector ControllerUpVector = MotionComps[1]->GetUpVector();
	FVector ControllerRightVector = MotionComps[1]->GetRightVector();

	float DotUpVector = FVector::DotProduct(MeshUpVector, ControllerUpVector);
	float DotRightVector = FVector::DotProduct(MeshUpVector, ControllerRightVector);

	if (DotUpVector >= -0.5f && DotUpVector <= 0.3f && DotRightVector >= 0.7f && DotRightVector <= 1.0f)
		return true;

	return false;
}
