// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MotionCapture.h"
#include "GameFramework/Character.h"
#include "TestCharacter.generated.h"

UCLASS()
class MOTIONCAPTURE_API ATestCharacter : public ACharacter
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh")
	class UPoseableMeshComponent*		PoseableMesh;
	class UIMUReceiver*					arrIMU[BONE_END];
	//class USceneComponent
	FRotator							CalibrationRotation[BONE_END];
	FRotator							BoneRotation[BONE_END];

public:
	FName								arrBoneName[BONE_END];

public:
	ATestCharacter();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void		SetSensorRotation();
	FRotator	CaculateBoneRotation(const FRotator& BoneRotator, const FRotator& SensorRotator);

public:
	// 1초 후 Calibration 상태로 변경
	void Test();

public:
	UFUNCTION(BlueprintCallable, Category = Sensor)
	void		TestPrintLog(int32 iIndex, FRotator Rotation, FColor Color, float fTime = 0.f);

	UPROPERTY()
	FTimerHandle						CheckTimerHandle;


public:
	// Test : 지울 거
	void MoveForward(float Value);
	void MoveRight(float Value);


	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);
};
