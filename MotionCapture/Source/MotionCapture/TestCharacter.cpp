// Fill out your copyright notice in the Description page of Project Settings.


#include "TestCharacter.h"
#include "Components/PoseableMeshComponent.h"
#include "MyGameInstance.h"
#include "IMUReceiver.h"

// Sets default values
ATestCharacter::ATestCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	PoseableMesh = CreateDefaultSubobject<UPoseableMeshComponent>(TEXT("PoseableMesh"));
	arrIMU[BONE_R_ELBOW] = CreateDefaultSubobject<UIMUReceiver>(TEXT("lowerarm_l"));
	arrIMU[BONE_R_ARM] = CreateDefaultSubobject<UIMUReceiver>(TEXT("upperarm_l"));
	arrIMU[BONE_R_SPINE] = CreateDefaultSubobject<UIMUReceiver>(TEXT("spine_03"));

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_MESH(TEXT("SkeletalMesh'/Game/Mannequin/Character/Mesh/SK_Mannequin.SK_Mannequin'"));

	if (SK_MESH.Succeeded())
	{
		PoseableMesh->SetSkeletalMesh(SK_MESH.Object);
	}

	PoseableMesh->SetupAttachment(GetMesh());

	arrBoneName[BONE_R_ELBOW] = TEXT("lowerarm_l");
	arrBoneName[BONE_R_ARM] = TEXT("upperarm_l");
	arrBoneName[BONE_R_SPINE] = TEXT("spine_03");

	PoseableMesh->SetRelativeLocation(FVector(0, 0,-90));
	PoseableMesh->SetRelativeRotation(FRotator(0, 270.f, 0));

}

void ATestCharacter::BeginPlay()
{
	Super::BeginPlay();

	int32 Num = 0;
	for (auto& Receiver : arrIMU)
	{
		Receiver->IMU_Id = Num;
		Receiver->DebugLevel = 0;
		Receiver->bUseRawData = true;
		Num++;

		if (Num == 1)
		{
			Receiver->DebugLevel = 1;
			Receiver->bEnableCSVRecording = true;
			Receiver->RecorderFilename = TEXT("CalibrationTest");
		}
	}

	
	
	// GameInstance에서 최종 센서값 가져오기
	//SetSensorRotation();

	
	// T포즈 상태의 로테이션 값
	for (int i = 0; i < BONE_END; ++i)
	{
		if (arrBoneName[i].IsEqual("upperarm_l"))
		{
			FRotator rotation = PoseableMesh->GetBoneRotationByName(arrBoneName[i], EBoneSpaces::ComponentSpace);
			rotation.Pitch += 50.f;
			PoseableMesh->SetBoneRotationByName(arrBoneName[i], rotation, EBoneSpaces::ComponentSpace);
		}

		/*if (arrBoneName[i].IsEqual("spine_03"))
		{
				FRotator rotation = PoseableMesh->GetBoneRotationByName(arrBoneName[i], EBoneSpaces::ComponentSpace);
				rotation.Yaw += 50.f;
				PoseableMesh->SetBoneRotationByName(arrBoneName[i], rotation, EBoneSpaces::ComponentSpace);
		}*/

		BoneRotation[i] = PoseableMesh->GetBoneRotationByName(arrBoneName[i], EBoneSpaces::ComponentSpace);
	}
	
	// Test
	GetWorld()->GetTimerManager().SetTimer(CheckTimerHandle, this, &ATestCharacter::Test, 1.f, false, 1.f);
}

void ATestCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// 매 프레임마다 센서에서 나온 Rotation 값과, GameInstance에서 가져온 Rotation값을 연산하여
	// 캐릭터 본에 셋팅해준다.

	FRotator ResultRotation = FRotator::ZeroRotator;
	for (int i = 0; i < BONE_END; ++i)
	{
		if (arrBoneName[i].IsEqual("upperarm_l"))
		{
			//ResultRotation = BoneRotation[i] + arrIMU[i]->Rotation - CalibrationRotation[i];

			FRotator TestRotation = arrIMU[i]->Rotation - CalibrationRotation[i];
			//TestRotation.Pitch = -TestRotation.Pitch;
			ResultRotation = BoneRotation[i] + TestRotation;

			//ResultRotation = CaculateBoneRotation(BoneRotation[i], TestRotation);
		}
		else if (arrBoneName[i].IsEqual("lowerarm_l"))
		{
			FRotator TestRotation = arrIMU[i]->Rotation - CalibrationRotation[i];
			TestRotation.Pitch = -TestRotation.Pitch;
			
			ResultRotation = BoneRotation[i] + TestRotation;
			//ResultRotation = CaculateBoneRotation(BoneRotation[i], TestRotation);

		}
	/*	else if (arrBoneName[i].IsEqual("spine_03"))
		{
			FRotator TestRotation = arrIMU[i]->Rotation - CalibrationRotation[i];
			TestRotation.Pitch = -TestRotation.Pitch;
			ResultRotation = BoneRotation[i] + TestRotation;
		}*/
		else
		{
			FRotator TestRotation = (arrIMU[i]->Rotation - CalibrationRotation[i]);
			ResultRotation = BoneRotation[i] + TestRotation;
			//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("Bone >>> %s"), *ResultRotation.ToString()));

		}

		// 180도가 넘어가면 -로 바뀐다.
		
		PoseableMesh->SetBoneRotationByName(arrBoneName[i],
			ResultRotation, EBoneSpaces::ComponentSpace);
	}
}

void ATestCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATestCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATestCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ATestCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ATestCharacter::LookUpAtRate);
}

void ATestCharacter::SetSensorRotation()
{
	for (int i = 0; i < BONE_END; ++i)
	{
		CalibrationRotation[i] = Cast<UMyGameInstance>(GetGameInstance())->InitSensorRotation[i];
	}
}

FRotator ATestCharacter::CaculateBoneRotation(const FRotator & BoneRotator, const FRotator & SensorRotator)
{
	FRotator ResultRotator;
	FQuat BoneQuat(BoneRotator);
	FQuat SensorQuat(SensorRotator);

	FQuat ResultQuat = BoneQuat * SensorQuat;
	ResultRotator = FRotator(ResultQuat);

	//FVector tmpRot = BoneQuat.Euler() + SensorQuat.Euler();
	

	/*FRotator rot = FRotator(vecRot.Y, vecRot.Z, vecRot.X);
	FQuat quatRot(rot);
	targetTransform = FRotator(quatRot);

		FQuat qrot = targetTransform.Quaternion();
		FVector tmpRot = qrot.Euler();
		UE_LOG(LogSensorFusion, Warning, TEXT("id[%s] euler[%s] quat[%s]")
		, *pData->Data.SensorID, *tmpRot.ToString(), *qrot.ToString());*/

	return ResultRotator;
}


void ATestCharacter::TestPrintLog(int32 iIndex, FRotator Rotation, FColor Color, float fTime)
{
	GEngine->AddOnScreenDebugMessage(-1, fTime, Color, FString::Printf(TEXT("Sensor: %s, X: %f, Y: %f, Z: %f"),
		*arrIMU[iIndex]->GetName(), Rotation.Pitch, Rotation.Yaw, Rotation.Roll));
}

void ATestCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ATestCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ATestCharacter::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * 45.f * GetWorld()->GetDeltaSeconds());
}

void ATestCharacter::LookUpAtRate(float Rate)
{
	AddControllerPitchInput(Rate * 45.f * GetWorld()->GetDeltaSeconds());
}

void ATestCharacter::Test()
{
	for (int i = 0; i < BONE_END; ++i)
	{
		CalibrationRotation[i] = arrIMU[i]->Rotation;
	}
	for (int i = 0; i < BONE_END; ++i)
		TestPrintLog(i, (arrIMU[i]->Rotation), FColor::Blue, 10.f);

	for (int i = 0; i < BONE_END; ++i)
		TestPrintLog(i, (CalibrationRotation[i]), FColor::Blue, 10.f);
}
