// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MotionCapture.h"
#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MOTIONCAPTURE_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	FRotator			InitSensorRotation[BONE_END];

public:
	void				InitRotation(FRotator* InitRotator);
	
};
