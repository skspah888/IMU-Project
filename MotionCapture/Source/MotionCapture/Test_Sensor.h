// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MotionCapture.h"
#include "GameFramework/Actor.h"
#include "Test_Sensor.generated.h"

UENUM(BlueprintType)
enum class EERROR_STATE : uint8
{
	ERRRO_NONE = 0			UMETA(DisplayName = "ERRRO_NONE"),
	IMU_SENSOR				UMETA(DisplayName = "IMU_SENSOR"),
	LEFT_CONTROLLER			UMETA(DisplayName = "LEFT_CONTROLLER"),
	RIGHT_CONTROLLER		UMETA(DisplayName = "RIGHT_CONTROLLER"),
	STATE_END				UMETA(DisplayName = "STATE_END")
};

UCLASS()
class MOTIONCAPTURE_API ATest_Sensor : public AActor
{
	GENERATED_BODY()


public:
	USceneComponent*					SceneComponent;
	class UIMUReceiver*					arrIMU[BONE_END];
	FRotator							arrRotator[BONE_END];
	FRotator							arrInitRotator[BONE_END];
	FRotator							arrCalibrationRotator[BONE_END];


	UPROPERTY()
	FTimerHandle						CheckTimerHandle;

	float								fTimerInterval;

	UPROPERTY(BlueprintReadWrite, Category = Sensor)
	int32								iCount;

	UPROPERTY(BlueprintReadWrite, Category = Sensor)
	bool								IsFinish;

	UPROPERTY(BlueprintReadWrite, Category = Sensor)
	EERROR_STATE						eState;

	UPROPERTY(EditAnywhere, Category = Sensor)
	APawn*								CalibrationPawn;


	TArray<class UMotionControllerComponent*> MotionComps;
	TArray<class USkeletalMeshComponent*> MeshComps;

public:	
	ATest_Sensor();

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	virtual void Tick(float DeltaTime) override;

public:
	UFUNCTION(BlueprintCallable, Category = Sensor)
	void		InitRotation();

	UFUNCTION(BlueprintCallable, Category = Sensor)
	void		TestPrintLog(int32 iIndex, FRotator Rotation, FColor Color, float fTime=0.f);

	UFUNCTION(BlueprintCallable, Category = Sensor)
	bool		CheckCalibration();

	UFUNCTION(BlueprintCallable, Category = Sensor)
	void		CheckCalibration_Count();

	UFUNCTION(BlueprintCallable, Category = Sensor)
	bool		CompareSensorRotation(const FRotator& InitRotator, const FRotator& CalibrationRotator);

	UFUNCTION(BlueprintCallable, Category = Sensor)
	bool		CompareMotionController();

	bool		Check_LeftController();
	bool		Check_RightController();

};
